function getData(){
    return new Promise((resolve) => {
    var xhr=new XMLHttpRequest();
    xhr.open('GET' , './data.json',true );
    xhr.onreadystatechange = function(){
         if (xhr.readyState == 4) {
              if(xhr.status == 200) {
                 resolve(JSON.parse(xhr.responseText));
              }
         }
    }
    xhr.send();})
    
} 

const data1 = `[
    {
        "id" : 0,
        "name" : "kursup",
        "category":0,
        "image": "./image.kursup.jpg",
        "description": "Описание блюда",
        "price": 10
    },

    {
        "id" : 1,
        "name" : "gribsup",
        "category":0,
        "image": "gribsup.jpg",
        "description": "Описание блюда",
        "price": 12
    },

    {
        "id" : 2,
        "name" : "otbiv",
        "category":1,
        "image": "otbiv.jpg",
        "description": "Описание блюда",
        "price": 10
    },
    
    {
        "id" : 3,
        "name" : "shahluk",
        "category":1,
        "image": "shahluk.jpg",
        "description": "Описание блюда",
        "price": 10
    },

    {
        "id" : 4,
        "name" : "veshTort",
        "category":2,
        "image": "veshTort.jpg",
        "description": "Описание блюда",
        "price": 10
    },


    {
        "id" : 5,
        "name" : "kokpip",
        "category":2,
        "image": "kokpip.jpg",
        "description": "Описание блюда",
        "price": 10
    },

    {
        "id" : 6,
        "name" : "Fanta",
        "category":3,
        "image": "Fanta.jpg",
        "description": "Описание блюда",
        "price": 10
    },

    {
        "id" : 7,
        "name" : "pepsi",
        "category":3,
        "image": "pepsi.jpg",
        "description": "Описание блюда",
        "price": 10
    }

]`

//console.log(data1);
//const DATA= getData();
//data.then(a => console.log(a));


/*function temporary (){
    return new Promise((resolve)=>{
    setTimeout(()=>{
     resolve (5);
    },5000); // время отклика 
});
}

var result= temporary();
//console.log(result);
result.then(function(param){
    console.log(param);
})*/

const CATEGORIES =
[
    {
        id: 0,
        name: 'Первые блюда'
    },
    {
        id: 1,
        name: 'Вторые блюда'
    },
    {
        id: 2,
        name: 'Третье блюда'
    },

    {
        id: 3,
        name: 'Напитки'
    }
];


var dishCounter= 1;
var selectedDish;
var cart = [];

function generaterFilterButten(){
    var filter = get('filter');//получаем фильтр из штмл
    CATEGORIES.forEach(category =>{
      var button = document.createElement('div');
      button.className = 'filter__button';
      button.innerText= category.name;
      button.addEventListener('click',() =>{filterDishes(category.id)});
      filter.appendChild(button);
    });

}

 function help (e){    // Метод остановки распростарнения закрытия окна, что бы внутри не закрывалось окно 
     e.stopPropagation();
 }

function filterDishes(id){
   var result = DATA.filter(item =>item.category==id);
   generateDishes(result);
}

function generateDish(item){
    var content = get('content__menu');

    var dish = 
    `<div class="dish">
    <img  class="dish__img" src="./image/${item.image}" alt="">
    <div class="dish__info">
        <div class="dish__info-text">${item.name}<br>${item.description}</div>

        <div class="dish__info-control">
            <span>Цена ${item.price} руб</span>
        <div class="dish__info-control-button"onclick="buy(${item.id})"> Купить</div>
        </div>
    </div>

</div>`;
content.innerHTML += dish;

}

function displayOrder( product){    // Метод отрисовки после нажатия кнопки купить 
         const item = get('modal__window-item');
         const card = ` <div class="info">
         <img class="info__img" src="./image/${product.image}" alt="">
         <div class="info__description">${product.description}</div>
        </div>
     <div class="action">
     <div class="action__price">
     <span> цена товара<b>${product.price} pуб.</b></span>
    <span  >Общая стоимость <b class="total-summ"> 1</b> </span>
     </div>
    <div class="action__confirm">
    <div class="counter">
                 <span class="counter__action" onclick="decrementCounter()"> -</span>
                 <input class="counter__inp" type="text" onkeyup="checkInput(this,event)">
                 <span class="counter__action" onclick="incrementCounter()">+</span>
             </div>
             <div class=" confirm-button" onclick="confirmOrder()"> Подтвердить заказ</div>
         </div>
     </div>`;
     item.innerHTML=card;
}


function confirmOrder(){    // Метод отрисовки после нажатия кнопки купить 
  cart.push(selectedDish); // push добавляет в конец массива элемент 
  closeModal();
  console.log(cart);
}

function checkInput(input, event){
    console.log(event)
    if(+event.key >=0 && + event.key < 10)
    { if (input.value>10) input.value=10;
    return;
    }
    input.value = 1;
}
function getTotalSum(dish){
    const summ = dish.price*dishCounter;
    const TotalSum = get('total-summ');
    TotalSum.innerText=summ;
}

function incrementCounter(){  
    if(dishCounter<10){ 
    dishCounter++;  // Метод отрисовки после нажатия кнопки купить 
    }
    sendCounter();
    getTotalSum(selectedDish);
}
function decrementCounter(){ 
    if(dishCounter>1){    // Метод отрисовки после нажатия кнопки купить 
    dishCounter--;
    }
    sendCounter();
    getTotalSum(selectedDish);
}


function sendCounter(){
    const input = get('counter__inp');
    input.value = dishCounter;
}


function buy(itemid){
    selectedDish = DATA.find(item => item.id == itemid);
    displayOrder(selectedDish);
    getTotalSum(selectedDish);

    openModal();
}
function generateDishes(dishes){
    var content = get('content__menu');
    content.innerHTML = '';
    dishes.forEach(dish =>{
        generateDish(dish);
     });
}


function closeModal(){
    const modal = get('modal');
    modal.style.display = 'none';
    dishCounter =1;

}
function openModal(){
    const modal = get('modal');
    modal.style.display = 'flex';
    sendCounter();
    
}
var get = function(name){
    return document.getElementsByClassName(name)[0];
}

 var selectedCategory = CATEGORIES[0].id; //отвечает за категорию которая выбрана 


var allDishes;


var DATA = JSON.parse(data1);
window.onload = function(){
    generaterFilterButten();

    //data1.then(result =>{       

    
    var filterDishes =DATA.filter(item => item.category== selectedCategory);
       //  var dishes =DATA.filter(item => item.category==2);
      //   generateDish();

         
         // console.log(dishes);
        generateDishes(filterDishes);
   
}

window.onload = function(){
    document.getElementById('close').onclick = function(){
        this.parentNode.parentNode.parentNode
        .removeChild(this.parentNode.parentNode);
        return false;
    };
};